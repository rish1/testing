import axios from 'axios';

export default axios.create({ baseURL: 'api.10trove.com/api/v1'});