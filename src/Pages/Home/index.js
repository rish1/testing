import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { getTroves } from '../../redux/trove';

function Home (props) {
  useEffect(() => {
    props.getMyTroves(props.token);
  }, []);
  return (
    <section>
      <h1>Home</h1>
    </section>
  );
}

const mapStateToProps = (state) => {
  return {
    token: state.user.token,
    loading: state.trove.loading,
    error: state.trove.error,
    troves: state.trove.troves,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMyTroves: (token) => dispatch(getTroves(token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)( Home);