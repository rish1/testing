import { troveActions } from "./actions";
import API from "../config/axios";
export const getTroves = (token) => {
  return (dispatch) => {
    dispatch({ type: troveActions.GET_TROVES });
    API.get("/troves", { headers: { "X-AUTH-TOKEN": token } })
      .then(({ data }) => {
        dispatch({ type: troveActions.GOT_TROVES, payload: { data } });
      })
      .catch((e) => {
        console.error(e);
        dispatch({
          type: troveActions.GET_TROVES_ERROR,
          payload: { errorMesssage: "Some message" },
        });
      });
  };
};

const initState = {
  loading: false,
  error: null,
  troves: []
};
export default function (state = initState, action) {
  switch (action.type) {
    case troveActions.GET_TROVES:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case troveActions.GOT_TROVES:
      return {
        ...state,
        loading: false,
        error: null,
        troves: action.payload.troves,
        page: action.payload.page,
      };
    case troveActions.GET_TROVES_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.errorMessage,
        troves: [],
        page: action.payload.page,
      };
    default:
      return state;
  }
}
