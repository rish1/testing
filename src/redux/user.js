const initState = {
  loading: false,
  token: null,
};
export default function(state=initState, action) {
  switch(action.type) {
    case "GET_TOKEN":
      return {
        ...state,
        token: action.payload.token,
      };
    default:
      return state;
  }
}