import { combineReducers } from 'redux';
import user from './user';
import trove from './trove';
export default combineReducers({
  user,
  trove
});
